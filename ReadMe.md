# Rabbla
En liten "app" som hjälper dig att träna matte.

## Hur gör man?
* Välj vilket tabell du vill
* Svara genom att skriva in det svar du tror är rätt och tryck på "Klar!"
* Svarar du rätt så blir texen grön, svarar du fel så blir den röd och kommer att komma igen.

För att klara en tabell så måste man klara av alla tal från 0 till 10. Svarar man fel så kommer talet att åter komma tills det att man svarat rätt.  


Eventuella hittade fel och förslag på förbättringar kan rapporteras här.
https://bitbucket.org/Skjaar/rabbla/issues?status=new&status=open 