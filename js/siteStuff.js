let currentNumber;
let nextNumber;
let activeTable;
let activeQuestion;
let answer = "";
let scrambledList = [];
let correctAnswer;
let wrongAnswer;
let currentHistory;

function loadTable(product) {
    let tempArray = Array();
    for (let i = 0; i < 11; ++i) {
        tempArray.push(i);
    }
    scrambledList = shuffle(tempArray);

    correctAnswer = 0;
    wrongAnswer = 0;
    currentHistory = new Date().getTime();
    nextNumber = 0;
    currentNumber = product;
    activeQuestion = `${scrambledList[nextNumber]} x ${currentNumber} = `;
    $("#result").text(activeQuestion);
    $("#nextInLine").html('<div class="text-primary"><h2>Tabell: ' + product + '</h2><div id="' + currentHistory + '"></div></div>');
    // $("#history").prepend('<div class="text-primary"><h2>Tabell: ' + product + '</h2><div id="' + currentHistory + '"></div></div>');
}

function shuffle(array) {
    let copy = [], n = array.length, i;

    // While there remain elements to shuffle…
    while (n) {

        // Pick a remaining element…
        i = Math.floor(Math.random() * array.length);

        // If not already shuffled, move it to the new array.
        if (i in array) {
            copy.push(array[i]);
            delete array[i];
            n--;
        }
    }

    return copy;
}

function addWrongAnswerToSecondNext(question) {
    console.log("Adding missed number: " + question);
    scrambledList.push(question);
}

$(document).ready(function() {

    for (let i = 0; i < 13; ++i) {
        // Adds event to table.
        $("#tabel_" + i).click(function() {
            activeTable = i;
            loadTable(i);
        })
    }

    for (let i = 0; i < 10; ++i) {
        $("#button_" + i).click(function() {
            if (nextNumber < scrambledList.length) {
                $("#result").append(i);
                answer = answer + "" + i;
            }
        });
    }

    $("#button_c").click(function() {
        if (nextNumber < scrambledList.length) {
            $("#result").text(activeQuestion + answer.substring(0, answer.length - 1));
            answer = answer.substring(0, answer.length - 1);
        }

        // $("#progressBar").text("");
        // nextNumber = 0;
    });

    $("#button_answer").click(function() {

        if (nextNumber === 0) {
            console.log("NextNumber = 0");
            $("#history").prepend($("#nextInLine").html());
            $("#nextInLine").text("")
        }

        if (nextNumber < scrambledList.length && answer) {

            // $("#result").text(activeQuestion);
            console.log((scrambledList[nextNumber] * currentNumber) + " " + answer);
            if ((scrambledList[nextNumber] * currentNumber) === (answer * 1)) {
                // $("#progressBar").append('<div class="progress-bar progress-bar-striped progress-bar-animated bg-success" role="progressbar" style="width: 9%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="120"></div>');
                correctAnswer++;
                console.log("Correct answer");
                $("#" + currentHistory).prepend('<div class="text-success">' + activeQuestion + answer + '</div>');
            } else {
                // $("#progressBar").append('<div class="progress-bar progress-bar-striped progress-bar-animated bg-warning" role="progressbar" style="width: 9%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="120"></div>');
                addWrongAnswerToSecondNext(scrambledList[nextNumber]);
                wrongAnswer++;
                console.log("Wrong answer");
                $("#" + currentHistory).prepend('<div class="text-danger">' + activeQuestion + answer + '</div>');
            }
            answer = "";
            ++nextNumber;
            if (nextNumber < scrambledList.length) {
                activeQuestion = `${scrambledList[nextNumber]} x ${currentNumber} = `;
                $("#result").text(activeQuestion);
            } else {
                $("#result").text(" Bra jobbat! ");
                $("#" + currentHistory).prepend("Bra jobbat! Du klarade alla");
            }
        }
    });
});
